module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy("src/icons/");
    return {
        dir: {
            input: "src",
            output: "public",
            data: "data",
            includes: "includes"
        }
    }
};