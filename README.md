To develop on this repository, run:

```sh
npm install -g gulp-cli # install Gulp.js tools
npm install # Install Elevent and other dependencies
gulp # Launch a static server with live reloading
```

To build the site, use `gulp build`.