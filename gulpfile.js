var gulp  = require('gulp');
var stylus = require('gulp-stylus');
var sourcemaps = require('gulp-sourcemaps');
var shell = require('gulp-shell');
var concat  = require('gulp-concat');


project = {
    buildDest: './public',
    buildSrc: './src'
};

gulp.task('styles', function() {
    return gulp.src(project.buildSrc + '/styles/index.styl')
        .pipe(sourcemaps.init())
        .pipe(stylus({
            compress: true
        }))
        .pipe(concat('bundle.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(project.buildDest + '/'));
});

gulp.task('watch', function () {
    gulp.watch(project.buildSrc + '/styles/**/*', gulp.parallel('styles'));
});

gulp.task('eleventyBuild', shell.task('eleventy', {
    quiet: false
}));
gulp.task('eleventyServe', shell.task('eleventy --serve', {
    quiet: false
}));
gulp.task('build', gulp.series(
  'eleventyBuild',
  'styles'
));
gulp.task('serve', gulp.parallel(
    'eleventyServe',
    'styles',
    'watch'
));

gulp.task('default', gulp.series('serve'));